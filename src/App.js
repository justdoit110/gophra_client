import React, { Component } from 'react';
import { connect } from "react-redux"
import { Route, Redirect, Switch } from 'react-router-dom'

import { fetchMenu } from "./actions"

import AppBar from './components/AppBar';
import CategoryView from './components/CategoryView';
import AddCategory from './components/AddCategory';
import AddPhrase from './components/AddPhrase';
import PhrasesList from './components/PhrasesList';
import MainMenu from './components/MainMenu';

import './css/App.css';
import AddList from './components/AddList';
import cookie from 'react-cookies';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {}    
  }  

  componentWillMount(){
    this.props.fetchMenu()
  }

  render() {
    const {categories} = this.props.menu
    
    if( cookie.load('token') === undefined ){
      return (<Redirect to="/auth" />)
    }
   
    return (
      <div>
        <AppBar 
          menuItems={categories}
          history={this.props.history}
        />
        <div className="App">             
          <Switch>
            <Route  
              exact path="/" 
              render={()=> <MainMenu menuItems={categories} /> } 
            />
            <Route 
              exact path="/category/add" 
              component={(props) => <AddCategory appParams={this.state} {...props} />} 
            />
            <Route 
              exact path="/list/add" 
              component={(props) => <AddList appParams={this.state} {...props} />} 
            />
            <Route 
              exact path="/phrase/add:x(\#)?:cat_id*" 
              component={(props) => <AddPhrase appParams={this.state} {...props} />} 
            />             
            <Route 
              exact path="/category/:category_id" 
              component={(props)=> <CategoryView  categories={categories} {...props} />} 
            />
            <Route 
              exact path="/category/:categoryId/:listId" 
              component={(props) => <PhrasesList appParams={this.state} {...props} />} 
            />
             
            
            {this.props.children}
          </Switch>
        </div>
      </div>
    );
  }
}

// export default App;

function mapStateToProps(state){  
  return {
    menu: state.menu
  }
}
const mapDispatchToProps = {
  fetchMenu
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

