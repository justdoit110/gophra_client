import React      from 'react';
import ReactDOM   from 'react-dom';
import {Provider} from "react-redux";
import cookie     from 'react-cookies'
import thunk      from 'redux-thunk'

import { Router, Route, Redirect, Switch }  from 'react-router-dom'
import { createBrowserHistory }               from "history"
import { createStore, applyMiddleware }   from "redux";
import {composeWithDevTools}              from 'redux-devtools-extension'


import App    from './App';
import Auth   from './components/Auth'
import SignIn from './components/SignIn'
import SignUp from './components/SignUp'
import Logout from './components/Logout'

import allReducers    from "./reducers"

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import TestComponent    from './components/TestComponent';
import './index.css'

const store = createStore(allReducers, composeWithDevTools(
  applyMiddleware(thunk)
));
const history = createBrowserHistory();

const api_token = cookie.load('token') || undefined;

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={props => (
    api_token ? (
      <Component {...props} api_token={api_token}/>
    ) : (
      <Redirect to={{
        pathname: '/auth',
        state: { from: "homepage" }
      }}/>
    )
  )}/>
);

ReactDOM.render(
  <Provider store={store}>
  <MuiThemeProvider>
    <Router history={history} >
    <Switch>      
      <Route exact path="/signin" component={SignIn} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/auth" component={Auth} />
      <Route exact path="/test" component={TestComponent} />
      <Route exact path="/logout" component={Logout} />
      <PrivateRoute path="/" history={history} component={App} />
      
    </Switch>
    </Router>
  </MuiThemeProvider>
  </Provider>
  , document.getElementById('root'));

// registerServiceWorker();
