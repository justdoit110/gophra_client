import { getMenu, addCategoryApi,
   addPhrasesApi, getPhrasesApi, addListApi } from '../api'

export const fetchMenu = ( ) => async dispatch => {

  try{
    let categories = await getMenu()
    
    categories = prepareCategories(categories)
    
    dispatch({
      type: "SAVE_MENU_DATA",
      payload: categories
    })
    
  } catch(err) {
    console.error("FETCH ERROR. Something went wrong fetchMenu: ", err)
  }  
}


export const addCategory = async ( data ) => {
  try {
    const resp = await addCategoryApi(data)    
    return resp
  } catch(err) {
    console.error("Something went wrong Category not added: ", err)
  }

}

export const addList = async ( data ) => {
  try {
    const resp = await addListApi(data)    
    return resp
  } catch(err) {
    console.error("Something went wrong List not added: ", err)
  }

}

export const addPhrases = async ( data ) => {
  try {
    return await addPhrasesApi(data)
    
  } catch(err) {
    console.error("Something went wrong Phrases not added: ", err)
    return {error: "network error"}
  }

}

export const getPhrases =  ( data ) => {
  try {
    return getPhrasesApi(data)
  } catch(err) {
    console.error("Something went wrong getPhrasesApi : ", err)
  }

}

/**
 * из массива категорий создает такойже массив только индексом 
 * элемента устанавливается id категории. со списками(внутри категории) делается также
 * @param {*} categories 
 */
function prepareCategories(categories){
  
  if(categories && categories.length !== 0){
    let res = []
    categories.forEach(el => {
      
      if(el.lists.length !== 0){
        let lists = []
        el.lists.forEach(list => {          
          lists[list.id] = list          
        })
        el.lists = lists
      }
      res[el.id] = el      
    });
    return res
  }
  return []
}