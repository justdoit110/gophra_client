import { appConfig }  from '../app.conf'
import cookie         from 'react-cookies'
import axios          from 'axios'


/**
 * API запрос на авторизацию
 * @param {*} payload 
 */
export const authentication = async (payload) => {

  return axios.post( appConfig.loginUrl, payload,{
    headers: { "Content-type": "application/json" },    
  } )
    .then( response => {

      return {status: response.status, data: response.data}
     
    })
    .catch( error => {
      return {status: 500, data: {error: "reques failed"}}      
    })
}


/*  =================================================================  */
/*  =================================================================  */
export const firstLevelCategoriesApi = async () => {
  
  const url = appConfig.firstLevelCategoriesUrl
  const response = await fetch( url, reqOptions() )
  return await response.json()
}

export const getPhrasesApi = async (data) => {

  const {reqUrl, page, listId} = data
  const url = reqUrl || appConfig.phrasesApiUrl+listId+"/phrases?page="+page

  const response = await fetch( url, reqOptions() )
  return await response.json()
}


/**
 * возвращает "дерево" категорий с подкатегориями
 */
export const getMenu = async () => {
  
    const url = appConfig.menuUrl
    const response = await fetch( url, reqOptions() )
    return await response.json()
}

/**
 * Добавляет категорию 
 * @param {*} data 
 */
export const addCategoryApi = async (data) => {
  
    const url = appConfig.addCategoryUrl
    const response = await fetch( url, reqOptions( 'POST', data ) )
    return await response.json()
}

/**
 * Добавляет Список в Категорию 
 * @param {*} data 
 */
export const addListApi = async (data) => {
  
  const url = appConfig.listApiUrl
  const response = await fetch( url, reqOptions( 'POST', data ) )
  return await response.json()
}

/**
 * Добавляет фразу (набор фраз) в список
 * @param {*} data 
 */
export const addPhrasesApi = async (data) => {
  
    const url = appConfig.addPhrasesApiUrl.replace("{listId}", data.listId)
    
    const response = await fetch( url, reqOptions( 'POST', {phrases: data.phrases} ))
    return await response.json()
}

/**
 * helper, формирует параметры (данные) запроса
 * @param {*} method 
 * @param {*} body 
 */
const reqOptions = (method='GET', body={}) => {
  let options = {
    method,    
    headers: reqHeaders()
  }
  method!=='GET' && (options.body = JSON.stringify(body) )

  return options
}

/** 
 * helper, формирует headers с авторизационным токеном
 */
const reqHeaders = () => {
  const headers = new Headers()
  headers.append('Content-Type', 'application/json')
  headers.append('Authorization', 'Bearer '+cookie.load('token'))

  return headers
}

