import { ENV } from './env'

let apiUrl = ( ENV === 'local' )? "http://localhost:8080/api/" : "http://apigophra.myfons.ru/api/"

export const appConfig =  {    
    signUpApiUrl: apiUrl+"register",
    loginUrl: apiUrl+"login",
    menuUrl: apiUrl+"menu",
    phrasesApiUrl: apiUrl+"list/",
    addPhrasesApiUrl: apiUrl+"list/{listId}/phrases",
    listApiUrl: apiUrl+"list",
    firstLevelCategoriesUrl: apiUrl+"first_level_sections",
    addCategoryUrl: apiUrl+"category"
};