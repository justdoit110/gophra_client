/**
 * вариант сообщения ошибки
 * вызывается с "контекстом" компонента содежращего './components/Notification' * 
 */
export function failureNotifiaction(message="Something went wrong") {
  return showNotification(message, 'error')  
}

/**
 * вариант сообщения "Успешно ..."
 * вызывается с "контекстом" компонента содежращего './components/Notification' * 
 */
export function successNotifiaction(message="Success"){
  return showNotification(message)
}

/** базовый метод отображения сообщения */
function showNotification(message, variant='success'){
  return { 
    notifyOpenState: true,
    notificationMessage: message,
    notificationVariant: variant 
  }
}