import React, { Component } from 'react'

import RaisedButton from 'material-ui/RaisedButton';

import '../css/App.css';
import { Link } from 'react-router-dom'


export default class Auth extends Component {
  render() {
    return (       
      <div className="authBlock">
      <div>
        <RaisedButton label="Sign In" primary={true} containerElement={<Link to="/signin" />} className="sideMargin" />        
        <RaisedButton label="Sign Up" primary={true} containerElement={<Link to="/signup" />} className="sideMargin" />
      </div>
      </div>         
    )
  }
}
