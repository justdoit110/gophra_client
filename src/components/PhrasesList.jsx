import React, { Component } from 'react'
import { connect } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'
import $ from 'jquery'
import { appConfig }  from '../app.conf'

import { Paper, Typography, withStyles } from '@material-ui/core';
// import Typography from '@material-ui/core/Typography';

import PhraseItem from './PhraseItem'
import { getPhrases } from "../actions"

import {isEmpty} from 'ramda'


import '../css/phrases.css'

class PhrasesList extends Component {

  constructor(props){
    super(props)

    this.state = { list: null }
    // this.state = {}
  }
 
  // prepareData(){

  //   // console.log("PhraseList props", this.props)
  //   /* listId есть когда открываем подКатегорию*/
  //   const {categoryId, listId } = this.props.match.params    
  //   const { categories } = this.props.menu    
  //   const list = categories[categoryId].lists[listId]
    
  //   this.setState({
  //     phrases: [],
  //     hasMoreItems: true,
  //     nextHref: null,      
  //     list:  list,
  //     page: 1
  //   })
  // }

  async getPhrases(page) {
    let args = {
      reqUrl: this.state.nextHref,
      page: this.state.page,
      listId: this.state.list.id
    }

    let response = await getPhrases(args)

    let phrases = this.state.phrases

    response.records.map( (item) => {
        return phrases.push(item)
      })

    if(response.current_page < response.total_pages) {
      this.setState({
        ...this.state,
        phrases, 
        nextHref: appConfig.phrasesApiUrl+this.state.list.id+'/phrases?page='+(+response.current_page+1)
      })
    } else {
      this.setState({
        ...this.state,
        phrases, 
        hasMoreItems: false
      })
    }

  }

  static getDerivedStateFromProps(props, state){
    
    // если в проспах ещё нет данных о меню(категориях)
    // ждем их из redux store и проинициализируем this.state 
    if(!isEmpty(props.menu) && state.list === null ) {
     
      const {categoryId, listId } = props.match.params    
      const { categories } = props.menu    
      const list = categories[categoryId].lists[listId]
    
      return {
        phrases: [],
        hasMoreItems: true,
        nextHref: null,      
        list:  list,
        page: 1
      }
    }
    return state
  }
 

  createPhrasesList( phrases ){
    phrases = this.state.phrases.map( (item, i) => {      
      return <PhraseItem phrase={item.phrase} translate={item.translate} key={item.id} />
    })
    return phrases
  }
  
  render() { 
    if(this.state.list === null /*isEmpty(this.props.menu) */) {
      return <div>wait...</div>
    }

    const { classes } = this.props

    return (
      <div className="phraseList" >
        <Paper className={classes.listTitle}  elevation={1}>
          <Typography variant="h5" component="h3">
            {this.state.list.title || ''}
          </Typography>
        </Paper>
            
        <InfiniteScroll
        pageStart={0}
        loadMore={this.getPhrases.bind(this)}
        hasMore={this.state.hasMoreItems}
        loader={<div className="loader" key="unwarning">Loading ...</div>}
        >
          {this.createPhrasesList()} 
        </InfiniteScroll>              
      </div>
    )
  }
}

const styles = theme => ({
  listTitle: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    margin: '20px 0'
  },
});

function mapStateToProps(state){  
  return {    
    menu: state.menu
  }
}

const PhrasesComponent = withStyles(styles)(PhrasesList)

export default connect(mapStateToProps)(PhrasesComponent);


$(document).on("scroll", ()=>{
  
  $("div.itemTranslate").each( function(){
      // console.log(  $(window).scrollTop() );
      if( ($(this).offset().top - $(window).scrollTop()) < (document.documentElement.clientHeight*0.45) ) {
          $(this).css('opacity', '1');
      }else {
          $(this).css('opacity', '0');

      }
  } );

  if( $(window).scrollTop() === 0 ) {
      $("div.itemTranslate").css('opacity', '0');
  }
  if( $(window).scrollTop() === ( $(document).height()-$(window).height() ) ) {
      $("div.itemTranslate").css('opacity', '1');
  }
})