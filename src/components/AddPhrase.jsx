import React, { Component } from 'react'
import { connect } from "react-redux"

import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import Notification from './Notification'
import {successNotifiaction, failureNotifiaction} from '../lib/methods'

import {isNil} from 'ramda'
// import classNames from 'classnames'
import { addPhrases } from "../actions"

class AddPhrase extends Component {
  constructor(props){
    super(props);
    this.state = {
      phrases: "",
      categoryId: '',
      listId: '',
      notificationMessage: '',
      notificationVariant: '',
      notifyOpenState: false,
      notificationTimeout: 2100,
    }
  }

  async formSubmit(event){    
    event.preventDefault();
    
    const {listId} = this.state;
    let res = await addPhrases( {listId, phrases: this.state.phrases})

    if(res.success){
      this.setState({ ...successNotifiaction(res.success), phrases: ""})
    }else if( res.error ){
      this.setState( failureNotifiaction(res.error) )
    }    
    
  }
    
  handleChangelist = (event) => {
    this.setState({listId: event.target.value })
  };

  handleChangeCategory = event => {
    this.setState({ categoryId: event.target.value });
  };

  render() {

    const { classes, menu } = this.props;

    if( isNil(menu.categories) ){
      return <div>waiting...</div>
    }

    const {categoryId} = this.state

    return ( 
      <div>
        <form method="post" onSubmit={this.formSubmit.bind(this)} >           
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="category-id">sel. category</InputLabel>
            <Select
              value={this.state.categoryId}
              onChange={this.handleChangeCategory}
              inputProps={{
                name: 'categoryId',
                id: 'category-id'
              }}
            >              
              {menu.categories.map((item)=>{
                return <MenuItem value={item.id} key={item.id}>{item.title}</MenuItem>
              })}
              
            </Select>
          </FormControl>
          <br/>
          <FormControl className={classes.formControl} { ...(!this.state.categoryId && {disabled:true }) } >
            <InputLabel htmlFor="list-id">sel. list</InputLabel>
            <Select
              value={this.state.listId}
              onChange={this.handleChangelist}
              inputProps={{
                name: 'listId',
                id: 'list-id'
              }}
            >              
              {Boolean(categoryId) && menu.categories[categoryId].lists.map((item)=>{
                return <MenuItem value={item.id} key={item.id}>{item.title}</MenuItem>
              })}
              
            </Select>
          </FormControl>
          <br/>
          <TextField
            id="phrases"            
            multiline
            value={this.state.phrases}
            rows={10}
            variant="outlined"
            label="phrases"
            fullWidth
            onChange={ (e) => this.setState({phrases: e.target.value}) }
          /><br /><br />
          <Button
            variant="contained" 
            color="primary"
            size="medium"
            onClick={this.formSubmit.bind(this)}
          > Add </Button>
        </form>
        <Notification 
          openStatus={this.state.notifyOpenState} 
          handleClose={()=>this.setState({ notifyOpenState: false })}
          message={this.state.notificationMessage}
          variant={this.state.notificationVariant}
          autoHideDuration={this.state.notificationTimeout}
        />
      </div>
    )
  }
}

const styles = theme => ({ 
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 190,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

 function mapStateToProps(state){  
  return {
    menu:     state.menu
  }
}


 const AddPhraseComponent = withStyles(styles)(AddPhrase);


 export default connect(mapStateToProps)(AddPhraseComponent)