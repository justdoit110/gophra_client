import React, { Component } from 'react'
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';


export default class TestComponent extends Component {
  render() {    
    console.log("test component props: ", this.props);
    return (
      <div>
        <h3> I am TestComponent </h3>

        <SelectField            
            maxHeight={300}
            floatingLabelText="this is sparta"   
        >
          <MenuItem disabled={true} value={0} key={0} primaryText="item 0" />
          <MenuItem value={1} key={1} primaryText="- item 1" />
          <MenuItem value={2} key={2} primaryText="- item 2" />
        </SelectField>
      </div>
    )
  }
}
