import React, { Component } from 'react'

import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import { SnackbarContent, Snackbar} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import green from '@material-ui/core/colors/green';
import { withStyles } from '@material-ui/core/styles';

import classNames from 'classnames';

const variantIcon = {
  success: CheckCircleIcon,
  // warning: WarningIcon,
  error: ErrorIcon,
  // info: InfoIcon,
};

class Notification extends Component {

  render() {

    let { classes, openStatus, variant } = this.props
    
    // в идеале variant должен быть одним из ключей variantIcon
    // иногда в variant передается пустая строка - переопределим дефолтным значением
    variant = variant || 'success'
    const Icon = variantIcon[variant]
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={openStatus}
        autoHideDuration={ this.props.autoHideDuration || 1500}
        onClose={this.props.handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
      >
        <SnackbarContent
          className={classNames(classes[variant], 'success')}
          
          action={[            
            <IconButton
              key="close"
              aria-label="Close"
              color="inherit"
              onClick={this.props.handleClose}
            >
              <CloseIcon />
            </IconButton>,
          ]}
          message={
            <span id="message-id" className={classes.message}>
              <Icon className={classNames(classes.icon, classes.iconVariant)}/>
              {this.props.message || "Contact added"}
            </span>}
        />
        
      </Snackbar>
    )
  }
}

const styles = theme => ({
  success: {
    backgroundColor: green[600],
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },  
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit,
  },
  message: {
    display: 'flex',
    alignItems: 'center',
  },
});

export default withStyles(styles)(Notification);