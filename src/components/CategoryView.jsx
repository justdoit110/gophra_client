import React, { Component } from 'react'
import { Link } from 'react-router-dom'

import MenuItem from 'material-ui/MenuItem';

export default class CategoryView extends Component {

  constructor(props){
    super(props);
    // console.log("CategoryView props ", this.props)
    this.state = this.props.match.params  
  }

  subCategories( items, category ){

    this.menu = Object.values(items).map( (item, i) => {      
      return (<MenuItem 
        primaryText={item.title} 
        key={item.id}  
        containerElement={<Link to={{ pathname: '/category/'+category+'/'+item.id }} />}         
      /> )
    });
    return this.menu
  }

  render() {

    // console.log("match params", this.props.match.params)
     if( this.props.categories !== undefined ){
      
      const category = this.props.categories[ this.state.category_id ];

      if ( category.lists.length === 0 ){
        this.menu = <div> В этом разделе нет списков </div>
      }else { 
        this.subCategories( category.lists, this.state.category_id)        
      }
      this.categoryTitle = category.title
    }

    return (
      <div>
        <h3>{this.categoryTitle}</h3>
        {this.menu}        
      </div>
    )
  }
}

  // function mapStateToProps( state ) {
  //   return {
  //     menu: state.menu_data,
  //     authData: state.auth
  //   }
  // }  
 
 
  // export default connect(mapStateToProps)(CategoryView);
