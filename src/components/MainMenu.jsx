import React from 'react'
import MenuItem from 'material-ui/MenuItem';
import { Link } from 'react-router-dom'

const MainMenu = props => {
    
  const createMainMenu = () => {
    
    // last first    
    let items = Object.values(props.menuItems)
    items.sort((a, b) => parseFloat(b.id) - parseFloat(a.id))
    
    let menu = items.map( (item, i) => {      
      return (<MenuItem 
        primaryText={item.title} 
        key={item.id}  
        containerElement={<Link to={{ pathname: '/category/'+item.id }} />}             
      /> )
    })
    return menu
  }

  if( props.menuItems ){
    return createMainMenu()
  } else {
    return <p>Loading... </p>
  }
}
export default MainMenu