import { Component } from 'react'
import cookie from 'react-cookies';

export default class Logout extends Component {
  render() {    
    localStorage.removeItem("appData")
    cookie.remove('token')
    return window.location.href = "/"
  }
}
