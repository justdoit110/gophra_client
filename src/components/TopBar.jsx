import React, { Component } from 'react'
import { Link, Redirect } from 'react-router-dom'

import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import ArrowDropRight from 'material-ui/svg-icons/navigation-arrow-drop-right';

import '../css/App.css';


const Logged = (props) => (
  <IconMenu
  {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon /></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
    // useLayerForClickAway={true}

  >
    <MenuItem primaryText="Home" containerElement={ ()=> <Link to="/" /> } />
    <MenuItem
    {...props}
      primaryText="Add"
      rightIcon={<ArrowDropRight />}
      menuItems={[
        <MenuItem 
          primaryText="Category" 
          containerElement={ ()=> <Link to="/category/add" /> }          
        />,
        <MenuItem          
          primaryText="Phrase" 
          containerElement={ ()=> <Link to="/phrase/add" />  }         
        />,
        <MenuItem 
          primaryText="simple"           
        />
      ]}
    />
    <MenuItem primaryText="Sign Out" containerElement={ ()=><Link to="/logout" /> }/>
  </IconMenu>
);
Logged.muiName = 'IconMenu'; // white color

export default class TopBar extends Component {

  constructor(props) {
    super(props) 
    this.state = {menuOpen: false};
    this.scrollPos = 0
  }
  
  /**
   * при скроле прячет/показывает topBar 
   */
  scrollHandler(e){
    // console.log("scrollTop", this )
    if(this.scrollPos < document.documentElement.scrollTop && document.documentElement.scrollTop > 0){
      this.bar.className = "app-bar hideBar"
      this.scrollPos = document.documentElement.scrollTop-15
    }else {
      this.bar.className = "app-bar fixBar"
      this.scrollPos = document.documentElement.scrollTop
    }   
    
  }

  componentDidMount() {
    // console.log("TopBar didMount")    
    window.addEventListener( "scroll", this.scrollHandler.bind(this))
  }

  componentWillUnmount(){
    // console.log("TopBar Unmount")
    
  }

  logOut() {
    console.log("log out")
    localStorage.removeItem("appData")
    window.location.href = "/"
  }

  render() {
    const {pathname} = this.props.history.location
    const {history} = this.props
    if(this.state.toHome)
      return <Redirect to={{ pathname: '/' }} />
    
    return (
      <div className="app-bar fixBar" ref={(appBar)=> {this.bar = appBar}} >
        <div >
        <AppBar
          className="material-app-bar"
          iconStyleLeft = {{display: 'none'}}
          iconElementRight = {<Logged /> }
          onTitleClick = { ()=> pathname==="/"? history.replace('/') : history.push("/")  }
          title="iFraza"
          // titleStyle={{lineHeight: '50px'}}
          style={{ textAlign: 'center', cursor: 'pointer' }}
        />
        </div>
        
      </div>
    )
  }
}

  // function mapStateToProps( state ) {
  //   return {
  //     menu: state.menu_data,
  //     authData: state.auth
  //   }
  // }
 
  // function mapDispatchToProps( dispatch ){
  //   return bindActionCreators({saveMenu: saveMenu}, dispatch)
  // }
 
 
  // export default connect(mapStateToProps, mapDispatchToProps)(TopBar);
/* <Drawer
  docked={false}
  width={200}
  open={this.state.menuOpen} 
  onRequestChange={(menuOpen) => this.setState({menuOpen})}
  style={styled}
>
  {this.menu}
</Drawer> */