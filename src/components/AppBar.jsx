import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'


import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import MoreIcon from '@material-ui/icons/MoreVert';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import classNames from 'classnames'

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
};

class TopAppBar extends Component {

  constructor(props){
    super(props)

    this.state = {
      anchorEl: null
    }
  }

  

   /**
   * при скроле прячет/показывает topBar 
   */
  scrollHandler(e){
    // console.log("scrollTop", this )
    if(this.scrollPos < document.documentElement.scrollTop && document.documentElement.scrollTop > 0){
      this.bar.className = "app-bar hideBar"
      this.scrollPos = document.documentElement.scrollTop-15
    }else {
      this.bar.className = "app-bar fixBar"
      this.scrollPos = document.documentElement.scrollTop
    }    
  }

  componentWillMount(){

  }

  componentDidMount() {
    // console.log("TopBar didMount")    
    window.addEventListener( "scroll", this.scrollHandler.bind(this))
  }

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render(){
  const { classes } = this.props;

  const { anchorEl } = this.state;
  const open = Boolean(anchorEl);
  return (
    <div className={classNames("app-bar", "fixBar", classes.root)} ref={(appBar)=> {this.bar = appBar}}>
      <AppBar position="static">
        <Toolbar>
          
          <Typography variant="h6" color="inherit" className={classes.grow}>
            {/* <Link to="/"> GoPhra </Link> */}
            <Button component={Link} to="/" style={{color: "white"}}>GoPhra</Button>
          </Typography>
          <IconButton 
            color="inherit"aria-owns={open ? 'render-props-menu' : undefined} 
            onClick={this.handleClick}
          >
            <MoreIcon />
            
          </IconButton>
          <Menu 
            open={open} 
            id="render-props-menu"
            anchorEl={anchorEl}
            onClose={this.handleClose}
          >
            <MenuItem component={Link} to="/category/add" onClick={this.handleClose} >Add Category</MenuItem>
            <MenuItem component={Link} to="/list/add" onClick={this.handleClose} >Add List</MenuItem>
            <MenuItem component={Link} to="/phrase/add" onClick={this.handleClose} >Add Phrase</MenuItem>
            <MenuItem component={Link} to="/logout" onClick={this.handleClose} >Logout</MenuItem>
            
          </Menu>
        </Toolbar>
      </AppBar>
    </div>
  );
  }

}

TopAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopAppBar);