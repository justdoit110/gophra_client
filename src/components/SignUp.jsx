import React, { Component } from 'react'
import axios from 'axios';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import {appConfig} from "../app.conf";




export default class SignUp extends Component {

  signupSubmit(event){
    
        event.preventDefault();
    
        // console.log( this.state );
        let { email, password } = this.state
        let payload = { email, password }
    
        axios.post(appConfig.signUpApiUrl, payload, {
          headers: { "Content-type": "application/json" }
        })
          .then( response => {
            localStorage.setItem( "appData", JSON.stringify( response.data ) );
            window.location.href = "/";
          })
        .catch( error => {
          this.setState( {...this.state, info: "Something went wrong, Look console"});
          console.log( "response API error: ", error );
          
        })

        // fetch( signUpApiUrl, payload )
        //   .then( response => {
    
        //     switch (response.status) {
        //       case 401:
        //         this.setState( {...this.state, info: "Wrong Credentials"});
        //         return false;
        //         break;
        //       case 422:
        //         this.setState( {...this.state, info: "All fields must be fill"});
        //         return false;
        //         break;
        //     }
        //     this.props.saveToken( response.data.api_token )
        //     // let data = {api_token: .api_token }
        //     localStorage.setItem( "appData", JSON.stringify( response.data ) )
        //     window.location.href = "/";
        //   })
          
        
      }

  render() {
    return (
      <div className="loginForm">
         <form method="post" onSubmit={this.signupSubmit.bind(this)} > 
            <TextField
              hintText="Your E-mail"
              floatingLabelText="E-mail"
              onChange={ (event,newValue) => this.setState({email:newValue})}
              />
            <br/>
            <TextField
              type="password"
              hintText="Password"
              floatingLabelText="Password"
              onChange = {(event, newValue) => this.setState({password: newValue})}
              />
            <br/>
            <RaisedButton label="Sign Up" primary={true} style={style} onClick={this.signupSubmit.bind(this)}/>
          </form>
      </div>
    )
  }
}

const style = {
  margin: 15,
 };