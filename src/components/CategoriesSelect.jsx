import React, { Component } from 'react'
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';


export default class CategoriesSelect extends Component {


  prepareItems(items){
    let ret = [];
    const {disableParentCategories} = this.props;

    Object.values(items).map( (category, index) => {
      const { submenu } = category;
      ret.push(  <MenuItem 
        value={category.id} 
        key={category.id} 
        primaryText={category.caption} 
        disabled={Boolean(submenu) || (!category.parent_id && disableParentCategories)}
      />);

      if( submenu ) {    
        Object.values(submenu).forEach( (item, index) => {
          ret.push( <MenuItem 
            value={item.id} 
            key={item.id} 
            primaryText={"- "+item.caption}             
          /> )
        })
      }

      return 1
    });
    return ret
  }

  render() {

    const {onChange, maxHeight, currentValue, floatingLabelText, items} = this.props;
    return (
      <div>
        <SelectField
            value={currentValue}
            // onChange={this.handleChange}
            maxHeight={maxHeight}
            floatingLabelText={floatingLabelText}
            style={{textAlign: 'left'}}
            onChange={ onChange }
          >
            {this.prepareItems(items)}
          </SelectField>
      </div>
    )
  }
}
