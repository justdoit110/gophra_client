import React, { Component } from 'react'
import { connect } from "react-redux"
import { Redirect } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames'

import Notification from './Notification'
import {successNotifiaction, failureNotifiaction} from '../lib/methods'

import {addCategory, fetchMenu } from "../actions"


class AddCategory extends Component {
  constructor(props){
    super(props);
    this.state = {
      categoryName: '',      
      value: null,
      redirectToCat: null,
      notificationMessage: '',
      notificationVariant: '',
      notifyOpenState: false,
      inputId: "outlined-dense",
      errorLabel: false,
      notificationTimeout: 2100, // время отображения notification'a
    }
  }

  componentDidMount(){
    //!this.props.menu.firstLevelCategories && this.props.fetchFirstLevelCategories()
  }

  async formSubmit(event){
    event.preventDefault();
    let resp = await addCategory({title: this.state.categoryName});
    
    if(resp.success){
      // this.props.fetchMenu()      
      this.setState( { ...successNotifiaction(resp.success), categoryName: ""},
      ()=>{
        setTimeout(()=>{
          this.props.fetchMenu()
        }, this.state.notificationTimeout)
      } )
      // if(this.state.categoryParent){
      //   this.setState({redirectToCat: resp.data })
      // }     
    } else if(resp.error){
      this.setState( {
        ...failureNotifiaction(resp.error), 
        inputId: "standard-error", 
        errorLabel: true} )
    }    
  }

  handleInput = (e) => {
    this.setState({ categoryName: e.target.value, 
      inputId: "outlined-dense", 
      errorLabel: false })
  }

  render() {
    // console.log("props TEST: ", this.state)
    const {redirectToCat} = this.state;

    const { classes } = this.props;

    if(redirectToCat!=null){
      return <Redirect push to={{
        pathname: `/phrase/add#${redirectToCat}`
      }}/>
    }
    
    return (
      <div>
        <h3>Add Category</h3>
        <form method="post" onSubmit={this.formSubmit.bind(this)} > 
         
          <TextField
            {...{error: this.state.errorLabel}}
            id={this.state.inputId}
            label="enter title"
            className={classNames(classes.textField, classes.dense)}
            onChange={ this.handleInput }
            margin="dense"
            variant="outlined"
            autoFocus={true}
          />
          <br/>         
          <Button
            variant="contained" 
            color="primary"
            size="medium"
            onClick={this.formSubmit.bind(this)}
            // onClick={()=>this.setState( successNotifiaction("Category created") )}
          > Add </Button>
        </form>
        <Notification 
          openStatus={this.state.notifyOpenState} 
          handleClose={()=>this.setState({ notifyOpenState: false })}
          message={this.state.notificationMessage}
          variant={this.state.notificationVariant}
          autoHideDuration={this.state.notificationTimeout}
        />
      </div>
    )
  }
}

const styles = theme => ({ 
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
});


const mapDispatchToProps = {
  fetchMenu
};

 const AddCategoryComponent = withStyles(styles)(AddCategory);

 export default connect(()=>{return {}}, mapDispatchToProps)(AddCategoryComponent)