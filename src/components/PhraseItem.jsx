import React from 'react'
import '../css/phrases.css'

const PhraseItem = props => {
  
  return (
    <div className="phrase-box">
      <div className="itemPhrase"> {props.phrase} </div>       
      <div className="itemTranslate"> {props.translate} </div>      
    </div>);
}
export default PhraseItem