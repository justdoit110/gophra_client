import React, { Component } from 'react'
import { connect } from "react-redux"
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

import Notification from './Notification'
import {successNotifiaction, failureNotifiaction} from '../lib/methods'

import {isNil} from 'ramda'
import classNames from 'classnames'

import {addList, fetchMenu } from "../actions"


class AddList extends Component {
  constructor(props){
    super(props);
    this.state = {
      categoryId: '',
      listName: '',
      notificationMessage: '',
      notificationVariant: '',
      notifyOpenState: false,
      notificationTimeout: 2100, // время отображения notification'a
    }
  }

  handleChangeCategory = event => {
    this.setState({ categoryId: event.target.value });
  }; 

  componentDidUpdate(prevProps, prevState) {
   
  }

  async formSubmit(event){    
    event.preventDefault();
    let resp = await addList({title: this.state.listName, category_id: this.state.categoryId});
        
    if(resp.success){
      
      // сначала покажем сообщение, потом, через this.state.notificationTimeout 
      // обновляем стор новыми данными. Если вызывать setState и fetchMenu друг за другом
      // то когда быстро сработает fetchMenu вызывается render() и сбрасывается
      // отображение notification'a
      this.setState( successNotifiaction(resp.success),
        ()=>{
          setTimeout(()=>{
            this.props.fetchMenu()
          }, this.state.notificationTimeout)
        } )   
    } else if(resp.error){
      const {error} = resp

      var errMessage =[]
      for(let key in error){
        error[key].forEach( item => {
          errMessage.push(item)
        })
      }

      this.setState( failureNotifiaction(errMessage.join("<br/>")) )
    }
    
  }

  render() {
   
    const { classes, menu } = this.props;

    if( isNil(menu.categories) ){
      return <div>waiting...</div>
    }

    return (
      <div>
        <h3>Add List</h3>
        <form method="post" onSubmit={this.formSubmit.bind(this)} > 
         
          <TextField
            id="outlined-dense"
            label="enter title"
            className={classNames(classes.textField, classes.dense)}
            onChange={ (e) => this.setState({listName: e.target.value}) }
            margin="dense"
            variant="outlined"
            autoFocus={true}
          />
<br/>
          <FormControl className={classes.formControl}>
            <InputLabel htmlFor="category-id">sel. category</InputLabel>
            <Select
              value={this.state.categoryId}
              onChange={this.handleChangeCategory}
              inputProps={{
                name: 'categoryId',
                id: 'category-id'
              }}
            >              
              {menu.categories.map((i)=>{
                
                return <MenuItem value={i.id} key={i.id}>{i.title}</MenuItem>

              })}
              
            </Select>
          </FormControl>
          <br/>  
          <Button
            variant="contained" 
            color="primary"
            size="medium"
            onClick={this.formSubmit.bind(this)}
          > Add </Button>
        </form>
        <Notification 
          openStatus={this.state.notifyOpenState} 
          handleClose={()=>this.setState({ notifyOpenState: false })}
          message={this.state.notificationMessage}
          variant={this.state.notificationVariant}
          autoHideDuration={this.state.notificationTimeout}
        />
      </div>
    )
  }
}

const styles = theme => ({ 
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  formControl: {
    margin: theme.spacing.unit,
    minWidth: 190,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

function mapStateToProps(state){  
  return {
    menu: state.menu    
  }
}
const mapDispatchToProps = {
  fetchMenu
};

 const AddListComponent = withStyles(styles)(AddList);

 export default connect( mapStateToProps, mapDispatchToProps)(AddListComponent)