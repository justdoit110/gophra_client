import React, { Component } from 'react'

import { connect } from "react-redux"
import { Redirect } from 'react-router-dom';

import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import {authentication} from '../api'

import "../css/loginForm.css"
import cookie from 'react-cookies';




class SignIn extends Component {

  constructor(props){
    super(props);

    this.state = {
      info: ""
    };
  }
  componentDidMount(){
    // console.log( this.props )
  }

	async loginSubmit(event){

    event.preventDefault();
    
    var payload = {
      email:    this.state.login,
      password: this.state.password
    }

    let res = await authentication(payload)
    
    switch (res.status) {
      case 401:
        this.setState( {...this.state, info: "Wrong Credentials"});
        return false;
      case 422:
        this.setState( {...this.state, info: "All fields must be fill"});
        return false;
      case 200:      
        cookie.save("token", res.data.token)
        window.location.href = "/"
        return 
      default:
        break;
    }
    
	}

  render() {

    if( cookie.load('token') !== undefined ){
      return (<Redirect to="/" />)
    }

    return (
    	<div className="loginForm">        
          <div>            
            <span>{this.state.info}</span>
            <form method="post" onSubmit={this.loginSubmit.bind(this)} > 
              <TextField
                hintText="Enter your Username"
                floatingLabelText="Username"
                onChange={ (event,newValue) => this.setState({login:newValue})}
                />
              <br/>
              <TextField
                type="password"
                hintText="Enter your Password"
                floatingLabelText="Password"
                onChange = {(event, newValue) => this.setState({password: newValue})}
                />
              <br/>
              <RaisedButton label="Submit" primary={true} style={style} onClick={this.loginSubmit.bind(this)}/>
            </form>
          </div>        
      </div>
    );
  }
}

const style = {
  margin: 15,
 };

 function mapStateToProps( state ) {
   return {
    authData: state.auth    
   }
 }

 export default connect(mapStateToProps)(SignIn);