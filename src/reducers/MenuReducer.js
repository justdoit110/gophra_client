export default (state = null, { type, payload }) => {
  switch(type){    
    case "SAVE_MENU_DATA":    
      return {
        ...state,
        categories: payload
      }
    default: 
      return {
        ...state,
      }
  }
}
  