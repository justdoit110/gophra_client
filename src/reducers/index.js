import {combineReducers} from "redux"
import MenuReducer from "./MenuReducer"

const allReducers = combineReducers({
    menu: MenuReducer
})

export default allReducers